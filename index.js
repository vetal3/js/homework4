let numFirst = prompt('Введите первое число');

while (numFirst === '' || Number(isNaN(numFirst))) {
    numFirst = prompt('Не корректно указано число!', numFirst);
}
numFirst = Number(numFirst);

let numSecond = prompt('Введите второе число');
while (numSecond === '' || Number(isNaN(numSecond))) {
    numSecond = prompt('Не корректно указано число!', numSecond);
}
numSecond = Number(numSecond);

let operator = prompt('Введите знак операции');
while (operator === '' || !/^[+-/*]+$/.test(operator)) {
    operator = prompt('Не корректно указан знак операции! Доступные толко "+ - * /"', operator);
}

function calcResult(numFirst, numSecond, operator) {
  switch (operator) {
    case '+':
        return numFirst + numSecond;
    case '-':
        return numFirst - numSecond;
    case '*':
        return numFirst * numSecond;
    case '/':
        return numFirst / numSecond;
  }
}

alert('Ответ: ' + calcResult(numFirst, numSecond, operator));